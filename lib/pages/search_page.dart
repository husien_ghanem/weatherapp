import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/city_weather_cubit.dart';

class SearchPage extends StatelessWidget {
  String cityName = "alexandaria";
  TextEditingController textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Search a City'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: TextField(
            controller: textEditingController,
            onSubmitted: (data) async {
              getWeather(data, context);
            },
            decoration: InputDecoration(
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 32, horizontal: 24),
              label: const Text('search'),
              suffixIcon: GestureDetector(
                  onTap: () async {
                    getweatherOnTap(context);
                  },
                  child: const Icon(Icons.search)),
              border: const OutlineInputBorder(),
              hintText: 'Enter a city',
            ),
          ),
        ),
      ),
    );
  }

  void getweatherOnTap(BuildContext context) {
        BlocProvider.of<CityWeatherCubit>(context).getWeatherOfCity(
        cityName: textEditingController.text);
    
    BlocProvider.of<CityWeatherCubit>(context).cityName =
        textEditingController.text;
    
    Navigator.pop(context);
  }

  void getWeather(String data, BuildContext context) {
    cityName = data;
    BlocProvider.of<CityWeatherCubit>(context)
        .getWeatherOfCity(cityName: cityName);

    BlocProvider.of<CityWeatherCubit>(context).cityName = cityName;
    Navigator.pop(context);
  }
}
