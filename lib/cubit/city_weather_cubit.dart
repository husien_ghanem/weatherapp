import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:weather_app/models/weather_model.dart';

import '../services/weather_service.dart';

part 'city_weather_state.dart';

class CityWeatherCubit extends Cubit<CityWeatherState> {
  CityWeatherCubit(this.weatherService) : super(CityWeatherInitial());
  WeatherService weatherService ;

  String? cityName;
  void getWeatherOfCity({required String cityName}) async {
    emit(LoadingCityWeather());
    try {
     WeatherModel weatherModel = await weatherService.getWeather(cityName: cityName);
   
      emit(CityWeatherSuccess(weatherModel: weatherModel));
    } catch (e) {
      emit(CityWeatherError());
    }
  }
}
