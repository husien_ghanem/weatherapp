// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'city_weather_cubit.dart';

@immutable
abstract class CityWeatherState {}

class CityWeatherInitial extends CityWeatherState {}

class LoadingCityWeather extends CityWeatherState {}

class CityWeatherSuccess extends CityWeatherState {
  WeatherModel weatherModel;
  CityWeatherSuccess({
    required this.weatherModel,
  });
}

class CityWeatherError extends CityWeatherState {}
